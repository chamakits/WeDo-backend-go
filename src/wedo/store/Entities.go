package store

import (
	"encoding/json"
	"fmt"
)

//Added the tag literals for mgo.
type UserWithTasks struct {
	User User `bson:"User"`
	Tasks []Task `bson:"Tasks"`
}

type UserTask struct {
	Name string
	Task Task
}

type User struct {
	Name string `bson:"Name"`
}

type Task struct {
	Id    int `bson:"Id"`
	Order int `bson:"Order"`
	Done  bool `bson:"Done"`
	Title string `bson:"Title"`
}
func UsersToJson(users []User) []byte {
	jsonBytes, err := json.Marshal(users)
	if err != nil {
		fmt.Println("Error marshalling list of users")
	}
	return jsonBytes
}


func UserNamesToJson(users []string) []byte {
	jsonBytes, err := json.Marshal(users)
	if err != nil {
		fmt.Println("Error marshalling list of users")
	}
	return jsonBytes
}


func TaskFromJsonString( jsonBytes []byte ) Task {
	var task Task
	err := json.Unmarshal(jsonBytes, &task)
	if err != nil {
		fmt.Println("Error unmarshalling tasks.")
	}
	return task
}



func TasksToJson(tasks []Task) []byte {
	jsonBytes, err := json.Marshal(tasks)
	if err != nil {
		fmt.Println("Error marshalling list of users")
	}
	return jsonBytes
}

func TaskToJson(task Task) []byte {
	jsonBytes, err := json.Marshal(task)
	if err != nil {
		fmt.Println("Error marshalling list of users")
	}
	return jsonBytes
}

func UserTaskFromJsonBytes(bytes []byte) UserTask {
	var userTask UserTask
	err := json.Unmarshal(bytes, &userTask)
	if err != nil {
		fmt.Println("Error unmarshalling user task")
	}
	return userTask
}