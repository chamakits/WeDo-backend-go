package store

import (

)

type Store interface {
	GetUsers() []string
	GetTasksForUserName(userName string) []Task
	AddTaskToUserByName(userName string, task Task) Task
	Save()
	Users() []User
	UpdateTask(userName string, taskToEdit Task)
}