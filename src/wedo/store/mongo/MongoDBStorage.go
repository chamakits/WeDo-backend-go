package mongo

import (
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
	"log"
	"fmt"
	"wedo/store"
)


type MongoDBStorage struct {
	Store map[string] []store.Task
	Session *mgo.Session
	Collection *mgo.Collection
}

func Init() MongoDBStorage {
	session, err := mgo.Dial("localhost");
	if err != nil {
		log.Fatalf("TERM:Failed to start mongodb connection\n"+
			"TERM:%v",err)
	}
	
	c := session.DB("test").C("Store")
	mongoStorage := MongoDBStorage{*new(map[string][]store.Task),session,c}
	fmt.Printf("MongoStorage: %+v\n",mongoStorage)
	return mongoStorage
}

func (mongoStr MongoDBStorage) GetUsers() []string{
	
	var userTasks []store.UserWithTasks
	var users []string
        //err = c.Find(bson.M{"name": "Ale"}).One(&result)
	find := mongoStr.Collection.Find(bson.M{})
	fmt.Printf("FIND:%+v\n",find)
	err := find.All(&userTasks)
	if err != nil {
		log.Fatalf("TERM: Problem finding mongo search.\n"+
			"TERM:%v",err)
	}
	for _, userTask := range userTasks {
		users = append(users, userTask.User.Name)
	}
	fmt.Printf("Users:%+v\n",users)
	fmt.Printf("Usertasks:%+v\n",userTasks)
	return users
}

func (mongoStr MongoDBStorage) GetTasksForUserName(userName string) []store.Task{
	userTasks:= store.UserWithTasks{}
	myQuery := bson.M{"User":bson.M{"Name":userName}}
	err := mongoStr.Collection.Find(myQuery).One(&userTasks)

	if err != nil{
		fmt.Printf("Error retrieving tasks\nERR:%+v\n",err)
	}
	
	return userTasks.Tasks
}

func (mongoStr MongoDBStorage) AddTaskToUserByName(userName string, task store.Task) store.Task {
	
	
	return store.Task{}
}

func (mongoStr MongoDBStorage) Save(){
	
}

//See this to undertand how to mongo
//http://denvergophers.com/2013-04/mgo.article
func (mongoStr MongoDBStorage) Users() []store.User{
	userTasks := *new([]store.UserWithTasks)
	users := *new([]store.User)
	user := store.UserWithTasks{}

	//Mongo cli query:
	//db.Store.find( {User: { $type : 3}  })
	myQuery := bson.M{"User":bson.M{"Name":"Omar"}}
	fmt.Printf("Query: %+v\n",myQuery)
	userErr :=  mongoStr.Collection.Find(myQuery).One(&user)
	if userErr != nil {
		fmt.Printf("Error finding On User.\n%+v\n",userErr)
	}
	fmt.Printf("@@@@@@Omar user: %+v\n",user)

	myQuery = bson.M{"User":bson.M{"$type":3}}
	fmt.Printf("Query: %+v\n",myQuery)
	
	find := mongoStr.Collection.Find(myQuery)
	fmt.Printf("FIND:%+v\n",find)
	err := find.All(&userTasks)
	if err != nil {
		log.Fatalf("TERM: Problem finding mongo search.\n"+
			"TERM:%v",err)
	}
	for _, userTask := range userTasks {
		users = append(users, userTask.User)
	}
	fmt.Printf("Users:%+v\n",users)
	fmt.Printf("Usertasks:%+v\n",userTasks)
	return users
}

func (mongoStr MongoDBStorage) UpdateTask(userName string, taskToEdit store.Task) {
	
}