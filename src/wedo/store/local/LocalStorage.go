package local

import (
	"fmt"
	"os"
	"io/ioutil"
	"encoding/json"
	"bytes"
	"wedo/store"
)

//var jsonPath string = "./data/db.json"
var jsonPath string = "./data/db-usrobj.json"

type LocalStorage struct {
	Store []store.UserWithTasks
}

func newLocalStorage() LocalStorage {
	localStorage := LocalStorage{*new([]store.UserWithTasks)}
	return localStorage
}

func localDBFileExists() (bool, os.FileInfo) {
	fileInfo, err := os.Stat(jsonPath)
	if err == nil {
		return true, fileInfo
	} 
	
	return false, nil
}

func initDBFromFile() LocalStorage {
	readDB, err := ioutil.ReadFile(jsonPath)
	if err != nil {
		fmt.Println("Error reading file. File not found.")
	}
	var storage LocalStorage
	err = json.Unmarshal(readDB,&storage)

	if err != nil {
		fmt.Println("Error unmarshalling db.")
		//storage = nil
	}
	fmt.Printf("DB: %+v;\n",storage)
	return storage	
}

func Init() LocalStorage {

	var localStorage LocalStorage
	
	exists, _ := localDBFileExists()
	if(exists){
		localStorage = initDBFromFile()
	} else {
		localStorage = newLocalStorage()
		users := usersFromJsonFile("./data/peopleCap.json")

		//Horrible assumption that file exists
		for _, userName := range users {
			localStorage.AddTasksByUsername(userName,TasksFromJsonFile("./data/tasks/" + userName + "Tasks.json"))
			//localStorage.Store[userName] = TasksFromJsonFile("./data/tasks/" + userName + "Tasks.json")
		}
	}
	return localStorage

}

func usersFromJsonFile(filePath string) []string {
	peopleJsonBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Println("Error reading file. File not found.")
	}
	var users []string
	err = json.Unmarshal(peopleJsonBytes, &users)
	if err != nil {
		fmt.Println("Error unmarshalling people.")
	}

	return users
}

func TasksFromJsonFile(filePath string) []store.Task {
	taskJsonBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Println("Error reading file. File not found.")
	}
	var tasks []store.Task
	err = json.Unmarshal(taskJsonBytes, &tasks)
	if err != nil {
		fmt.Println("Error unmarshalling tasks.")
		tasks = []store.Task{}
	}

	return tasks
}

func (localStr LocalStorage) GetUsers() []string {
	users := make([]string,0)
	for _, currentUserTask := range localStr.Store {
		users = append(users, currentUserTask.User.Name)
	}

	return users
}

func (localStr LocalStorage) GetTasksForUserName(userName string) []store.Task {	
	for index, userTasks := range localStr.Store {
		if userTasks.User.Name == userName {
			return localStr.Store[index].Tasks
		}
	}
	return *new([]store.Task)
}

func (localStr LocalStorage) AddTaskToUserByName(userName string, task store.Task) store.Task {
	
	for index, userTasks := range localStr.Store {
		if userTasks.User.Name == userName {
			task.Id = len(userTasks.Tasks)
			task.Order=task.Id
			localStr.Store[index].Tasks = append(localStr.Store[index].Tasks,task)
			return task
		}
	}

	return task
}

func (localStr LocalStorage) Save() {
	dbBytes, err := json.Marshal(localStr)
	var indentedBuffer bytes.Buffer
	json.Indent(&indentedBuffer,dbBytes,"","\t")
	if err != nil {
		fmt.Println("Error marshalling db.")
		fmt.Printf("%+v\n",err)
	}
	//ioutil.WriteFile(jsonPath,dbBytes,os.ModePerm)
	ioutil.WriteFile(jsonPath,indentedBuffer.Bytes(),os.ModePerm)
}

func (localStr LocalStorage) Users() []store.User {
	users := make([]store.User,0)
	for _, currentUserName := range localStr.GetUsers(){
		currentUser := store.User{currentUserName}
		users= append(users,currentUser)
	}
	return users
}

func (localStr LocalStorage) UpdateTask(userName string, taskToEdit store.Task) {
	for index, userTasks := range localStr.Store {
		if userTasks.User.Name == userName {
			localStr.Store[index].Tasks[taskToEdit.Id]=taskToEdit
			break
		}
	}
	
}

func (localStr LocalStorage) AddTasksByUsername(username string, tasks []store.Task){
	for index, userTasks := range localStr.Store {
		if userTasks.User.Name == username {
			localStr.Store[index].Tasks = tasks
		}
	}
}