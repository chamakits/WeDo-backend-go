package main

import (
	"fmt"
	"bytes"
	"github.com/gorilla/mux"
	"net/http"
	"wedo/store/local"
	"wedo/store"
)

var localStorage store.Store

func main() {
	//localStorage = store.InitLocalStorage()
	localStorage = local.Init()
	//localStorage = mongo.Init()
	r := mux.NewRouter()
	r.HandleFunc("/people/", peopleHandler)
	r.HandleFunc("/people-tasks/{personName}", tasksHandler)
	r.HandleFunc("/add-tasks/", addTaskHandler)
	r.HandleFunc("/edit-task/", editTaskHandler)
	http.Handle("/", r)

	http.ListenAndServe(":8123", nil)

}

func corsEnable(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	//fmt.Println("corsEnable")
}

func tasksHandler(w http.ResponseWriter, r *http.Request) {
	corsEnable(&w)
	vars := mux.Vars(r)
	personName := vars["personName"]
	content := store.TasksToJson(localStorage.GetTasksForUserName(personName))

	fmt.Fprintf(w, "%s", content)
}

func peopleHandler(w http.ResponseWriter, r *http.Request) {
	corsEnable(&w)
	jsonBytes := store.UsersToJson(localStorage.Users())

	fmt.Fprintf(w, "%s", jsonBytes)
}

func addTaskHandler(w http.ResponseWriter, r *http.Request) {
	corsEnable(&w)
	_, isOption := r.Header["Access-Control-Request-Method"]
	if isOption {
		fmt.Println("Is Option")
		return
	}
	buffer := bytes.NewBuffer([]byte{})
	buffer.ReadFrom(r.Body)
	taskBytes := buffer.Bytes()

	fmt.Println(string(taskBytes))

	userTask := store.UserTaskFromJsonBytes(taskBytes)
	newTask := userTask.Task
	
	//fmt.Println(personName+":"+string(taskBytes))
	returnedTask := localStorage.AddTaskToUserByName(userTask.Name, newTask)
	fmt.Fprintf(w,"%s",string(store.TaskToJson(returnedTask)))

	//fmt.Printf("%+v\n",localStorage.Store[localStorage.GetUserFromName(userTask.Name)])
	localStorage.Save()
}

func editTaskHandler(w http.ResponseWriter, r *http.Request) {
	corsEnable(&w)
	_, isOption := r.Header["Access-Control-Request-Method"]
	if isOption {
		fmt.Println("Is Option")
		return
	}

	buffer := bytes.NewBuffer([]byte{})
	buffer.ReadFrom(r.Body)
	taskBytes := buffer.Bytes()
	
	userTask := store.UserTaskFromJsonBytes(taskBytes)
	taskToEdit := userTask.Task
	//This is suppor unsafe.  It's assuming the client will 'play nice'.  Fix it.
	localStorage.UpdateTask(userTask.Name, taskToEdit)

	//fmt.Printf("TaskEdited:%+v\n",taskToEdit)
	
	fmt.Fprintf(w,"%s",string(store.TaskToJson(taskToEdit)))
	localStorage.Save()
}